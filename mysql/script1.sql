create database if not exists practica1;
use practica1;

/* if not exits*/

CREATE TABLE IF NOT EXISTS cliente (
    id_cliente   INTEGER  auto_increment primary key,
    nombre       VARCHAR(50),
    contrasena   VARCHAR(100)
);


CREATE TABLE IF NOT EXISTS producto (
    id_producto   INTEGER NOT NULL auto_increment primary key,
    nombre        VARCHAR(50),
    precio        INTEGER,
    ruta_foto     VARCHAR(100),
    descripcion   VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS usuario (
    id_usuario   INTEGER NOT NULL auto_increment primary key,
    nombre       VARCHAR(100),
    contrasena   VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS orden (
    id_orden             INTEGER auto_increment primary key,
    total                INTEGER,
    fecha                VARCHAR(30),
    direccion            VARCHAR(100),
    estado				 VARCHAR(100),
    usuario_id_usuario   INTEGER NOT NULL,
    foreign key (usuario_id_usuario) references usuario(id_usuario) on update cascade on delete cascade
    
);

CREATE TABLE IF NOT EXISTS pedido (
    cliente_id_cliente     INTEGER NOT NULL,
    producto_id_producto   INTEGER NOT NULL,
    orden_id_orden         INTEGER NOT NULL,
    foreign key (cliente_id_cliente) references cliente(id_cliente) on update cascade on delete cascade,
    foreign key (producto_id_producto) references producto(id_producto) on update cascade on delete cascade,
    foreign key (orden_id_orden) references orden(id_orden) on update cascade on delete cascade
);


/* comienzo de procedimientos almacenados*/
DELIMITER $$
CREATE PROCEDURE crear_clientes(
    IN nombre       VARCHAR(50),
    IN contrasena   VARCHAR(100))    
BEGIN
    insert into cliente(nombre,contrasena) values (nombre,contrasena);
END$$

/*CALL crear_clientes('Cesar', 'Prueba2');*/

DELIMITER $$
CREATE PROCEDURE ver_clientes(
    IN parametro_id_cliente integer)
BEGIN
    select * from cliente where id_cliente=parametro_id_cliente;
END$$

/*CALL ver_clientes(1);*/

DELIMITER $$
CREATE PROCEDURE borrar_cliente(
    IN parametro_id_cliente integer)
BEGIN
    Delete from cliente where id_cliente=parametro_id_cliente;
END$$

/*CALL borrar_cliente(2);*/


DELIMITER $$
CREATE PROCEDURE Actualizar_Cliente_nombre(
    IN parametro_id_cliente integer, in parametro_nombre varchar(50))
BEGIN
	update cliente set nombre=parametro_nombre where id_cliente=parametro_id_cliente;
END$$


DELIMITER $$
CREATE PROCEDURE Actualizar_Cliente_contra(
    IN parametro_id_cliente integer, in parametro_contra varchar(100))
BEGIN
	update cliente set contrasena=parametro_contra where id_cliente=parametro_id_cliente;
END$$

/* procedimientos para usuarios ***********************/

DELIMITER $$
CREATE PROCEDURE crear_usuario(
    IN nombre       VARCHAR(50),
    IN contrasena   VARCHAR(100))    
BEGIN
    insert into usuario(nombre,contrasena) values (nombre,contrasena);
END$$

/*CALL crear_usuario('Roberto', 'Prueba2');
CALL crear_usuario('Cesar', 'Prueba2');*/

DELIMITER $$
CREATE PROCEDURE Ver_Usuario(
    IN parametro_id_usuario integer)
BEGIN
    select * from usuario where id_usuario=parametro_id_usuario;
END$$

/*CALL ver_usuario(1);*/

DELIMITER $$
CREATE PROCEDURE borrar_usuario(
    IN parametro_id_usuario integer)
BEGIN
    Delete from usuario where id_usuario=parametro_id_usuario;
END$$

/*CALL borrar_usuario(2);*/

DELIMITER $$
CREATE PROCEDURE Actualizar_Usuario_nombre(
    IN parametro_id_usuario integer, in parametro_nombre varchar(100))
BEGIN
	update usuario set nombre=parametro_nombre where id_usuario=parametro_id_usuario;
END$$


DELIMITER $$
CREATE PROCEDURE Actualizar_Usuario_contra(
    IN parametro_id_usario integer, in parametro_contra varchar(100))
BEGIN
	update usuario set contrasena=parametro_contra where id_usuario=parametro_id_usuario;
END$$

/* PROCEDIMIENTO PARA VERIFICAR LA CONTRASEÑA Y cliente */

DELIMITER $$
CREATE PROCEDURE Verificacion_De_Cliente(
    IN parametro_id_cliente integer, in parametro_contrasena varchar(100))
BEGIN
	select IF(EXISTS(SELECT * FROM cliente WHERE id_cliente =  parametro_id_cliente AND  contrasena=parametro_contrasena),1,0);
END$$


/* PROCEDIMIENTO PARA VERIFICAR LA CONTRASEÑA Y USUARIO */

DELIMITER $$
CREATE PROCEDURE Verificacion_De_Usuario(
    IN parametro_id_usuario integer, in parametro_contrasena varchar(100))
BEGIN
	select IF(EXISTS(SELECT * FROM usuario WHERE id_usuario =  parametro_id_usuario AND  contrasena=parametro_contrasena),1,0);
END$$



/*************** PROCEDIMIENTOS PARA PRODUCTOS **************************/

DELIMITER $$
CREATE PROCEDURE Crear_Producto(
    IN p_nombre       VARCHAR(50),
    IN p_precio   	Integer,
    IN p_ruta_foto	VARCHAR(100),
	IN p_descripcion  VARCHAR(100))
BEGIN
    insert into producto(nombre,precio,ruta_foto,descripcion) values (p_nombre,p_precio,p_ruta_foto,p_descripcion);
END$$



/*call Crear_Producto('Pollo',40,'C:\roberto\imagen.png','Pollo frito');*/
/*call Crear_Producto('Carne Asada',45,'C:\roberto\imagen.png','Carne con bebida');*/


DELIMITER $$
CREATE PROCEDURE Leer_Producto()
BEGIN
    select * from producto;
END$$



DELIMITER $$
CREATE PROCEDURE Leer_Producto_UNitario(
    IN p_id_producto       integer)
BEGIN
    select * from producto where id_producto=p_id_producto;
END$$



DELIMITER $$
CREATE PROCEDURE Borrar_Producto(
    IN p_id_producto       integer)
BEGIN
    Delete from producto where id_producto=p_id_producto;
END$$


DELIMITER $$
CREATE PROCEDURE Actualizar_producto_nombre(
    IN p_id_producto integer, in parametro_nombre varchar(50))
BEGIN
	update producto set nombre=parametro_nombre where id_producto=p_id_producto;
END$$


DELIMITER $$
CREATE PROCEDURE Actualizar_producto_precio(
    IN p_id_producto integer, in parametro_precio integer)
BEGIN
	update producto set precio=parametro_precio where id_producto=p_id_producto;
END$$

DELIMITER $$
CREATE PROCEDURE Actualizar_producto_descripcion(
    IN p_id_producto integer, in parametro_nombre varchar(50))
BEGIN
	update producto set descripcion=parametro_nombre where id_producto=p_id_producto;
END$$


DELIMITER $$
CREATE PROCEDURE Actualizar_producto_ruta_foto(
    IN p_id_producto integer, in parametro_foto varchar(100))
BEGIN
	update producto set ruta_foto=parametro_foto where id_producto=p_id_producto;
END$$
/************************* ORDENES ************************************/


/* PRIMER PASO*/
DELIMITER $$
CREATE PROCEDURE Crear_orden(
    IN p_total integer,in p_fecha varchar(30),in p_direccion varchar(100),in p_id_usuario integer, in p_estado varchar(100))
BEGIN
    insert into orden(total,fecha,direccion,usuario_id_usuario,estado) values (p_total,p_fecha,p_direccion,p_id_usuario,p_estado);
END$$



/* SEGUNDO PASO  guardarlo en una variable para el cuarto pasos*/
DELIMITER $$
CREATE PROCEDURE Guardar_orden()
BEGIN
	SELECT @@identity AS id_orden_ingresada from orden;
END$$

/* Tercer  PASO */
DELIMITER $$
CREATE PROCEDURE Inserta_Pedido(
    IN p_id_cliente integer,in p_id_producto integer,in p_orden_id_orden integer)
BEGIN
    insert into pedido(cliente_id_cliente,producto_id_producto,orden_id_orden) values (p_id_cliente,p_id_producto ,p_orden_id_orden);
END$$

/* CUARTO PASO */

DELIMITER $$
CREATE PROCEDURE Total_orden(
    IN p_orden integer)
BEGIN
	select precio from producto where id_producto=(select id_producto where orden_id_orden=p_orden);
END$$


/* QUINTO PASO ACTUALIZAR EL PRECIO TOTAL*/
DELIMITER $$
CREATE PROCEDURE Actualizar_Total_orden(
    IN p_total integer,in p_orden integer)
BEGIN
	update orden set total=p_total where id_orden=p_orden;
END$$


/* SEXTO PASO ACTUALIZAR EL ESTADO*/
DELIMITER $$
CREATE PROCEDURE Actualizar_Estado_orden(
    IN p_estado varchar(100),in p_orden integer)
BEGIN
	update orden set estado=p_estado where id_orden=p_orden;
END$$

/* termina lo de ordenes */



DELIMITER $$
CREATE PROCEDURE Leer_Ordenes(
    IN p_orden integer)
BEGIN
	select * from orden where id_orden=p_orden;
END$$


DELIMITER $$
CREATE PROCEDURE Borrar_Orden(
    IN p_orden integer)
BEGIN
	Delete from orden where id_orden=p_orden;
END$$




