var express = require("express");
var router = express.Router();
const cors = require("cors");
let bodyParser = require("body-parser");
let urlencodedParser = bodyParser.json({ extended: false });
const DataBaseHandler = require("../models/db");
const handler = new DataBaseHandler();


const user = require("../models/user.model")


router.post("/Verproductos",(req2,res2)=>{
    
  handler.getConnection().query("SELECT * FROM producto;", (err,res)=>{
    if(err){
        console.log("ERROR: No se puede visualizar los productos  ",err);
        res2.send(err);
        return;
    }
    console.log("Resultado Busqueda ",res);
    res2.send(res);
});
})

router.post("/VerUnicoproducto",urlencodedParser,(req2,res2)=>{
  handler.getConnection().query("SELECT * FROM producto WHERE id_producto="+req2.body.id_producto+";", (err,res)=>{
    if(err){
        console.log("ERROR: No existe producto registrado  ",err);
        res2.send(err);
        return;
    }
    if(res[0]!=undefined){
    res2.send(res[0]);
    }else{
      res2.status(500).send("ERROR: No existe producto registrado")
    }
});
})

router.post("/IngresarProducto",urlencodedParser,(req2,res2)=>{
    
  handler.getConnection().query("INSERT INTO producto(nombre,precio,ruta_foto,descripcion) VALUES('"+req2.body.nombre+"',"+req2.body.precio+",'"+req2.body.ruta_foto+"','"+req2.body.descripcion+"');", (err,res)=>{
    if(err){
        console.log("ERROR: Problema Ingresando Productos  ",err);
        res2.send(err);
        return;
    }
    
    if(res.affectedRows==1){
    res2.send("Producto registrado");
    }else{
      res2.status(500).send("Producto No Registrado");
    }
});
})


router.post("/BorrarProducto",urlencodedParser,(req2,res2)=>{
    
  handler.getConnection().query("Delete From producto where id_producto="+req2.body.id_producto+";", (err,res)=>{
    if(err){
        console.log("ERROR: Problema Borrando producto",err);
        res2.send(err);
        return;
    }
    if(res.affectedRows==1){
    res2.send("Producto eliminado");
    }else{
      res2.status(500).send("No se elimino el producto")
    }
});
})


router.post("/ModificarProducto",urlencodedParser,(req2,res2)=>{
  
  for (var i = 0; i < 4; i++) {
    if(i==0){
      handler.getConnection().query("CALL Actualizar_producto_nombre("+req2.body.id_producto+",'"+req2.body.nombre+"');", (err,res)=>{
        if(err){
            i=5;
            console.log("ERROR: Problema Actualizando Nombre producto",err);
            res2.send(err);
            return;
        }
        if(res.affectedRows==1){
        //res2.send("Producto Actualizado");
        }else{
          i=5;
          res2.status(500).send("No se Actualizo el producto")
        }
    });
    }else if(i==1){
      handler.getConnection().query("CALL Actualizar_producto_precio("+req2.body.id_producto+","+req2.body.precio+");", (err,res)=>{
        
        if(err){
            i=5;
            console.log("ERROR: Problema Actualizando Nombre producto",err);
            res2.send(err);
            
            return;
        }
        if(res.affectedRows==1){
        //res2.send("Producto Actualizado1");
        }else{
          i=5;
          res2.status(500).send("No se Actualizo el producto")
        }
    });

    }else if(i==2){
      handler.getConnection().query("CALL Actualizar_producto_descripcion("+req2.body.id_producto+",'"+req2.body.descripcion+"');", (err,res)=>{
        if(err){
          i=5;  
            console.log("ERROR: Problema Actualizando Nombre producto",err);
            res2.send(err);
            return;
        }
        if(res.affectedRows==1){
        //res2.send("Producto Actualizado1");
        }else{
          i=5;
          res2.status(500).send("No se Actualizo el producto")
        }
    });

    }else if(i==3){
      handler.getConnection().query("CALL Actualizar_producto_ruta_foto("+req2.body.id_producto+",'"+req2.body.ruta_foto+"');", (err,res)=>{
        if(err){
          i=5;
            console.log("ERROR: Problema Actualizando Nombre producto",err);
            res2.send(err);
            return;
        }
        if(res.affectedRows==1){
        res2.send("Producto Actualizado");
        }else{
          i=5;
          res2.status(500).send("No se Actualizo el producto")
        }
    });
      
    }
    
 }


})



  module.exports = router;