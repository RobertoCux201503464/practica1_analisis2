module.exports = {
    HOST: process.env.MYSQL_HOST ? process.env.MYSQL_HOST : '172.17.0.2',
    USER: process.env.MYSQL_USER ? process.env.MYSQL_USER : 'root',
    PASSWORD: process.env.MYSQL_PASSWORD ? process.env.MYSQL_PASSWORD :'1234',
    DB: process.env.MYSQL_DATABASE ? process.env.MYSQL_DATABASE : 'practica1',
    PORT: process.env.MYSQL_PORT ? process.env.MYSQL_PORT : 33060,
  };