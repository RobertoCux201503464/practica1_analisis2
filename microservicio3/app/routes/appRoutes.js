'use strict';

module.exports = function(app) {
    
    var ordenes = require('../controller/appController');

    app.route('/')
        .get(ordenes.index);

    app.route('/actualizar_estado_orden')
        .put(ordenes.actualizar_estado_orden);

    app.route('/actualizar_total_orden')
        .put(ordenes.actualizar_total_orden);

    app.route('/borrar_orden')
        .delete(ordenes.borrar_orden);

    app.route('/crear_orden')
        .post(ordenes.crear_orden);

    app.route('/leer_orden')
        .get(ordenes.leer_todas_ordenes)
        .post(ordenes.leer_orden);
}