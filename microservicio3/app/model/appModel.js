'user strict';

var DataBaseHandler = require("./db");
var handler = new DataBaseHandler();

class Orden {

    static actualizar_estado_orden(estado, id_orden, result) {
        handler.getConnection().query("CALL Actualizar_Estado_orden(?, ?)", [estado, id_orden], function(err, res) {
            if (err) {
                console.log("Error: ", err);
                result(err, null);
            }
            else {
                console.log('Actalizado: ', res);
                result(null, res);
            }
        });
    }

    static actualizar_total_orden(total, id_orden, result) {
        handler.getConnection().query("CALL Actualizar_Total_orden(?, ?)", [total, id_orden], function(err, res) {
            if(err) {
                console.log("Error: ", err);
                result(err, null);
            }
            else {
                console.log('Actualizado: ', res);
                result(null, res);
            }
        });
    }

    static borrar_orden(id_orden, result) {
        handler.getConnection().query("CALL Borrar_Orden(?)", [id_orden], function(err, res) {
            if(err) {
                console.log("Error: ", err);
                result(err, null);
            }
            else {
                console.log('Borrado: ', res);
                result(null, res);
            }
        });
    }

    static crear_orden(total, fecha, direccion, id_usuario, estado, result) {
        handler.getConnection().query("CALL Crear_orden(?, ?, ?, ?, ?)", [total, fecha, direccion, id_usuario, estado], function(err, res) {
            if(err) {
                console.log("Error: ", err);
                result(err, null);
            }
            else {
                console.log('Creada: ', res);
                result(null, res);
            }
        });
    }

    static leer_orden(id_orden, result) {
        handler.getConnection().query("CALL Leer_Ordenes(?)", [id_orden], function(err, res) {
            if(err) {
                console.log("Error: ", err);
                result(err, null);
            }
            else {
                console.log('Orden: ', res[0]);
                result(null, res[0]);
            }
        });
    }

    static leer_todas_ordenes(result) {
        handler.getConnection().query("SELECT * FROM orden", function(err, res) {
            if(err) {
                console.log("Error: ", err);
                result(err, null);
            }
            else {
                console.log('Ordenes: ', res);
                result(null, res);
            }
        });
    }
}

module.exports = Orden;