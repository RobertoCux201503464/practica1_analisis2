'use strict';

var Orden = require('../model/appModel');

exports.index = function(req, res) {
    console.log('Ruta index');
    res.send({ message: 'Bienvenido al microservicio3 para las ordenes'});
};

exports.actualizar_estado_orden = function(req, res) {
    var body = req.body;
    var estado = body.estado;
    var id_orden = body.id_orden;

    Orden.actualizar_estado_orden(estado, id_orden, function(err, msg) {
        if(err) { res.send(err); }
        else {
            res.json({ message: msg });
        }
    });
};

exports.actualizar_total_orden = function(req, res) {
    var body = req.body;
    var total = body.total;
    var id_orden = body.id_orden;

    Orden.actualizar_total_orden(total, id_orden, function(err, msg) {
        if(err) { res.send(err); }
        else {
            res.json({ message: msg });
        }
    });
};

exports.borrar_orden = function(req, res) {
    var body = req.body;
    var id_orden = body.id_orden;

    Orden.borrar_orden(id_orden, function(err, msg) {
        if(err) { res.send(err); }
        else {
            res.json({ message: msg });
        }
    });
};

exports.crear_orden = function(req, res) {
    var body = req.body;
    var total = body.total;
    var fecha = body.fecha;
    var direccion = body.direccion;
    var id_usuario = body.id_usuario;
    var estado = body.estado;

    Orden.crear_orden(total, fecha, direccion, id_usuario, estado, function(err, msg) {
        if(err) { res.send(err); }
        else {
            res.json({ message: msg });
        }
    });
};

exports.leer_orden = function(req, res) {
    var body = req.body;
    var id_orden = body.id_orden;

    Orden.leer_orden(id_orden, function(err, msg) {
        if(err) { res.send(err); }
        else {
            res.send(msg);
        }
    });
};

exports.leer_todas_ordenes = function(req, res) {
    Orden.leer_todas_ordenes(function(err, msg) {
        if(err) { res.send(err); }
        else {
            res.send(msg);
        }
    });
};
