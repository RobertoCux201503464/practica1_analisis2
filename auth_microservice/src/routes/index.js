const express = require("express");
const router = express.Router();
const cors = require("cors");
const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.json({ extended: false });
const DataBaseHandler = require("../models/db.js");
const handler = new DataBaseHandler();

const user = require("../models/user.model")


router.post("/client/login",urlencodedParser,(req2,res2)=>{
    
  handler.getConnection().query("SELECT * FROM cliente WHERE nombre='"+req2.body.usuario+"' AND contrasena='"+req2.body.contrasena+"'", (err,res)=>{
    if(err){
        console.log("ERROR: Problema registrando datos de usuario  ",err);
        res2.send(err);
        return;
    }
    console.log("Resultado Login: ",res);
    if(res[0]!=undefined){
    res2.send(res[0]);
    }else{
      res2.status(500).send("No se logro el Login")
    }
});
})


router.post("/rest/login",urlencodedParser,(req2,res2)=>{
    
  handler.getConnection().query("SELECT * FROM usuario WHERE nombre='"+req2.body.usuario+"' AND contrasena='"+req2.body.contrasena+"'", (err,res)=>{
    if(err){
        console.log("ERROR: Problema registrando datos de usuario  ",err);
        res2.send(err);
        return;
    }
    console.log("Resultado Login: ",res);
    if(res[0]!=undefined){
    res2.send(res[0]);
    }else{
      res2.status(500).send("No se logro el Login")
    }
});
})

router.post("/client/registro",urlencodedParser,(req2,res2)=>{
    
  handler.getConnection().query("INSERT INTO cliente VALUES(null,'"+req2.body.nombre+"','"+req2.body.contrasena+"')", (err,res)=>{
    if(err){
        console.log("ERROR: Problema registrando datos de usuario  ",err);
        res2.send(err);
        return;
    }
    console.log("Usuario Registrado: ",res);
    if(res.affectedRows==1){
    res2.send("Usuario registrado");
    }else{
      res2.status(500).send("No se registro")
    }
});
})

router.post("/rest/registro",urlencodedParser,(req2,res2)=>{
    
  handler.getConnection().query("INSERT INTO usuario VALUES(null,'"+req2.body.nombre+"','"+req2.body.contrasena+"')", (err,res)=>{
    if(err){
        console.log("ERROR: Problema registrando datos de usuario  ",err);
        res2.send(err);
        return;
    }
    console.log("Usuario Registrado: ",res);
    if(res.affectedRows==1){
    res2.send("Usuario registrado");
    }else{
      res2.status(500).send("No se registro")
    }
});
})

router.get("/client/users",urlencodedParser,(req2,res2)=>{
    
  handler.getConnection().query("SELECT * FROM cliente", (err,res)=>{
    if(err){
        console.log("ERROR: Problema enviando datos de todos los usuarios  ",err);
        res2.send(err);
        return;
    }
    console.log("Usuarios: ",res);
    res2.send(res);
});
})


router.get("/rest/users",urlencodedParser,(req2,res2)=>{
    
  handler.getConnection().query("SELECT * FROM usuario", (err,res)=>{
    if(err){
        console.log("ERROR: Problema enviando datos de todos los usuarios  ",err);
        res2.send(err);
        return;
    }
    console.log("Usuarios: ",res);
    res2.send(res);
});
})

  module.exports = router;