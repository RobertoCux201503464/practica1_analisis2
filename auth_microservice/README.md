# Microservicio 1: Autenticación
Este microservicio es el encargado de gestionar a los usuarios que van a interactuar con el sistema. Este consiste de 3 funciones principales para cada tipo de usuario, los cuales son:
-  Login
-  Registro
-  Lista de Usuarios

Los tipos de usuarios son 2, un cliente consumidor el cual es un usuario que va a ser un usuario que hace pedidos, y por otro lado está el usuario restaurante, el cual es el encargado de crear menús para pedir. Esta API usa metodos `POST` para hacer los registros y logins, mientras una `GET` para obtener las listas de usuarios.



## Codigos de Respuesta
### Response Codes
```
200: Success
400: Bad request
401: Unauthorized
404: Cannot be found 
500: Error en la petición
```

### Mensaje de error
```json
http code 500
{
    "code": 500,
    "message": "Error en las credenciales"
}
```

## Login Cliente /client/login
**Se envia:**  Enviar las credenciales.
**Se recibe:** El objeto de la base de datos que cumpla con las credenciales de login.

**Request:**
```json
POST /login HTTP/1.1
Accept: application/json
Content-Type: application/json

{
    "nombre": "foo",
    "contrasena": "1234567" 
}
```
**Successful Response:**
```json
HTTP/1.1 200 OK
Server: My RESTful API
Content-Type: application/json
Content-Length: xy

{
    "id":"1",
   "nombre": "foo",
   "contrasena": "1234567"
}
```
**Failed Response:**
```json
HTTP/1.1 500 Unauthorized
Server: My RESTful API
Content-Type: application/json
Content-Length: xy

{
    "code": 500,
    "message": "invalid crendetials",
    "resolve": "The username or password is not correct."
}
``` 



## Login Restaurante /rest/login
**Se envia:**  Enviar las credenciales.
**Se recibe:** El objeto de la base de datos que cumpla con las credenciales de login.

**Request:**
```json
POST /login HTTP/1.1
Accept: application/json
Content-Type: application/json

{
    "nombre": "foo",
    "contrasena": "1234567" 
}
```
**Successful Response:**
```json
HTTP/1.1 200 OK
Server: My RESTful API
Content-Type: application/json
Content-Length: xy

{
    "id":"1",
   "nombre": "foo",
   "contrasena": "1234567"
}
```
**Failed Response:**
```json
HTTP/1.1 500 Unauthorized
Server: My RESTful API
Content-Type: application/json
Content-Length: xy

{
    "code": 500,
    "message": "invalid crendetials",
    "resolve": "The username or password is not correct."
}
``` 



## Registro Cliente /client/registro
**Se envia:**  Enviar las credenciales.
**Se recibe:** El objeto de la base de datos que cumpla con las credenciales de login.

**Request:**
```json
POST /login HTTP/1.1
Accept: application/json
Content-Type: application/json

{
    "nombre": "foo",
    "contrasena": "1234567" 
}
```
**Successful Response:**
```json
HTTP/1.1 200 OK
Server: My RESTful API
Content-Type: application/json
Content-Length: xy

{
    "code":200,
    "message":"Usuario Registrado"
}
```
**Failed Response:**
```json
HTTP/1.1 500 Unauthorized
Server: My RESTful API
Content-Type: application/json
Content-Length: xy

{
    "code": 500,
    "message": "invalid crendetials",
    "resolve": "Usuario no registrado."
}
``` 



## Registro Restaurante /rest/registro
**Se envia:**  Enviar las credenciales.
**Se recibe:** El objeto de la base de datos que cumpla con las credenciales de login.

**Request:**
```json
POST /login HTTP/1.1
Accept: application/json
Content-Type: application/json

{
    "nombre": "foo",
    "contrasena": "1234567" 
}
```
**Successful Response:**
```json
HTTP/1.1 200 OK
Server: My RESTful API
Content-Type: application/json
Content-Length: xy

{
    "code":200,
    "message":"Usuario Registrado"
}
```
**Failed Response:**
```json
HTTP/1.1 500 Unauthorized
Server: My RESTful API
Content-Type: application/json
Content-Length: xy

{
    "code": 500,
    "message": "invalid crendetials",
    "resolve": "Usuario no registrado."
}
``` 