import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'app/services/auth.service';
import { Usuario } from 'app/services/usuario';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  data: Date = new Date();
  focus_nombre: boolean;
  focus_contrasena: boolean;
  tipo_usuario: string = "cliente";
  nombre: string = '';
  contrasena: string = ''
  is_loggedin = false;
  constructor(private modalService: NgbModal, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    if(localStorage.getItem('usuario')){
      this.router.navigate(['/']);
    }
    var body = document.getElementsByTagName('body')[0];
    body.classList.add('login-page');
    var navbar = document.getElementsByTagName('nav')[0];
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('login-page');
    var navbar = document.getElementsByTagName('nav')[0];
    // navbar.classList.remove('navbar-transparent');
  }

  login(): boolean {
    if (this.nombre && this.nombre != '' && this.contrasena && this.contrasena != '') {
      if (this.tipo_usuario === 'cliente') {
        this.authService.ingresoCliente(new Usuario(this.nombre, this.contrasena))
          .subscribe(res => {
            if(res.status && res.status === 'offline'){
              return false;
            }
            this.is_loggedin = true;
            localStorage.setItem('usuario', JSON.stringify(res));
            localStorage.setItem('tipo_usuario',this.tipo_usuario);
            this.router.navigate(['/']);
            return true;
          });
      } else {
        this.authService.ingresoUsuario(new Usuario(this.nombre, this.contrasena))
          .subscribe(res => {
            if(res.status && res.status === 'offline'){
              return false;
            }
            this.is_loggedin = true;
            localStorage.setItem('usuario', JSON.stringify(res));
            localStorage.setItem('tipo_usuario',this.tipo_usuario);
            this.router.navigate(['/']);
            return true;
          });
      }
    }
    return false;
  }

  open(content: string) {
    setTimeout(()=>{
      if(!this.is_loggedin){
        this.modalService.open(content, { windowClass: 'modal-mini modal-primary', size: 'sm' }).result;
      }
    },100)
  }
}
