import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Producto } from 'app/services/producto';
import { ProductoService } from 'app/services/producto.service';
import { Subscription } from 'rxjs/Subscription';
import { Carrito } from 'app/Services/carrito';

@Component({
  selector: 'app-productpage',
  templateUrl: './productpage.component.html',
  styleUrls: ['./productpage.component.scss']
})
export class ProductpageComponent implements OnInit {
  data: Date = new Date();

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  dropdownList1 = [];
  selectedItems1 = [];
  dropdownSettings1 = {};
  id = 0;
  producto: Producto = new Producto();
  private routeSub: Subscription;
  constructor(private route: ActivatedRoute, private productService: ProductoService) { }

  ngOnInit() {
    this.routeSub = this.route.params.subscribe((params: { id: number }) => {
      this.id = params.id;
      this.productService.obtenerProducto(this.id).subscribe(
        res => {
          this.producto = res;
        }
      );
    });

    var body = document.getElementsByTagName('body')[0];
    body.classList.add('product-page');
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');
  }

  get esUsuario(): boolean {
    return localStorage.getItem('tipo_usuario') === 'usuario';
  }

  ngOnDestroy() {
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('product-page');
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
  }
  addToCart() {
    let carrito: Carrito = new Carrito();
    if(localStorage.getItem('carrito')!== null){
      carrito.elementos = JSON.parse(localStorage.getItem('carrito')).elementos;
    }
    carrito.add(this.producto);
    localStorage.setItem('carrito', JSON.stringify(carrito));
    console.log(carrito)
  }
}
