import { Component, OnInit } from '@angular/core';
import { Orden } from 'app/Services/orden';
import { OrdenService } from 'app/Services/orden.service';

@Component({
  selector: 'app-ordenes',
  templateUrl: './ordenes.component.html',
  styleUrls: ['./ordenes.component.scss']
})
export class OrdenesComponent implements OnInit {
  ordenes = new Array();
  constructor(private ordenService:OrdenService) { }

  ngOnInit() {
    this.ordenService.obtenerOrdenes().subscribe(res=>{
      this.ordenes = res;
      console.log(this.ordenes);
    });
  }

}
