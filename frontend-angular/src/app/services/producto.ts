export class Producto {
    id_producto?: number
    nombre: string
    precio: number
    ruta_foto: string
    descripcion: string
}
