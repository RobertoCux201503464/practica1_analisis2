export class Usuario {
    constructor(private usuario:string, private contrasena:string){
    }
    get registro() {
        return { nombre: this.usuario, contrasena: this.contrasena }
    }
    get login() {
        return { usuario: this.usuario, contrasena: this.contrasena }
    }
}
