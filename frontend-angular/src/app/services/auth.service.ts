import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Usuario } from './usuario';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // baseUrl = 'http://localhost:80'; // Para pruebas
  baseUrl = '';
  constructor(private http: HttpClient) { }

  registrarUsuario(user: Usuario): Observable<any> {
    return this.http.post(this.baseUrl + '/api/auth/rest/registro', JSON.stringify(user.registro), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(
        catchError(this.handleError('getStatus1', { status: 'offline' }))
      );
  }

  registrarCliente(user: Usuario): Observable<any> {
    return this.http.post(this.baseUrl + '/api/auth/client/registro', JSON.stringify(user.registro), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(
        catchError(this.handleError('getStatus1', { status: 'offline' }))
      );
  }

  ingresoUsuario(user: Usuario): Observable<any> {
    return this.http.post(this.baseUrl + '/api/auth/rest/login', JSON.stringify(user.login), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(
        catchError(this.handleError('getStatus1', { status: 'offline' }))
      );
  }

  ingresoCliente(user: Usuario): Observable<any> {
    return this.http.post(this.baseUrl + '/api/auth/client/login', JSON.stringify(user.login), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(
        catchError(this.handleError('getStatus1', { status: 'offline' }))
      );
  }

  obtenerUsuarios(user: Usuario): Observable<any> {
    return this.http.get(this.baseUrl + '/api/auth/rest/users')
      .pipe(
        catchError(this.handleError('getStatus1', { status: 'offline' }))
      );
  }

  obtenerClientes(user: Usuario): Observable<any> {
    return this.http.get(this.baseUrl + '/api/auth/client/users')
      .pipe(
        catchError(this.handleError('getStatus1', { status: 'offline' }))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   * */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
