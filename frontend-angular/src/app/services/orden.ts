export class Orden {
    id_orden?: number;
    total: number;
    fecha: Date;
    direccion: string;
    id_usuario: number;
    estado: string;
}
