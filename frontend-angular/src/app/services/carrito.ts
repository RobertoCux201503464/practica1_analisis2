import { Producto } from "./producto";

export class Carrito {
    elementos: { producto: Producto, cantidad: number }[] = new Array();
    get total(): number {
        let val = 0;
        for (let i = 0; i < this.elementos.length; i++) {
            val += this.elementos[i].producto.precio * this.elementos[i].cantidad;
        }
        return val;
    }
    add(producto: Producto) {
        for (let i = 0; i < this.elementos.length; i++) {
            if (this.elementos[i].producto.id_producto === producto.id_producto) {
                this.elementos[i].cantidad++;
                return;
            }
        }
        this.elementos.push({ producto, cantidad: 1 })
    }
    clear() {
        this.elementos.length = 0;
    }
}
