import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Orden } from './orden';
@Injectable({
  providedIn: 'root'
})
export class OrdenService {
  // baseUrl = 'http://localhost:80'; // Para pruebas
  baseUrl = '';
  constructor(private http: HttpClient) { }
  
  obtenerOrdenes(): Observable<any> {
    return this.http.get(this.baseUrl + '/api/ordenes/leer_orden')
      .pipe(
        catchError(this.handleError('getStatus1', { status: 'offline' }))
      );
  }

  obtenerOrden(orden: Orden): Observable<any> {
    return this.http.post(this.baseUrl + '/api/ordenes/leer_orden', JSON.stringify(orden), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(
        catchError(this.handleError('getStatus1', { status: 'offline' }))
      );
  }

  crearOrden(orden: Orden): Observable<any> {
    return this.http.post(this.baseUrl + '/api/ordenes/crear_orden', JSON.stringify(orden), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(
        catchError(this.handleError('getStatus1', { status: 'offline' }))
      );
  }

  actualizarEstadoOrden(orden: Orden): Observable<any> {
    return this.http.post(this.baseUrl + '/api/ordenes/actualizar_estado_orden', JSON.stringify(orden), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(
        catchError(this.handleError('getStatus1', { status: 'offline' }))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   * */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
