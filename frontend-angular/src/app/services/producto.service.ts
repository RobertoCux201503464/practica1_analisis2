import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Producto } from './producto';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {
  // baseUrl = 'http://localhost:80'; // Para pruebas
  baseUrl = '';
  constructor(private http: HttpClient) { }

  registrarProducto(prod: Producto): Observable<any> {
    return this.http.post(this.baseUrl + '/api/productos/IngresarProducto', JSON.stringify(prod), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(
        catchError(this.handleError('getStatus1', { status: 'offline' }))
      );
  }

  obtenerProductos(): Observable<any> {
    return this.http.post(this.baseUrl + '/api/productos/Verproductos', JSON.stringify({}), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(
        catchError(this.handleError('getStatus1', { status: 'offline' }))
      );
  }

  obtenerProducto(id_producto: number): Observable<any> {
    return this.http.post(this.baseUrl + '/api/productos/VerUnicoproducto', JSON.stringify({ id_producto }), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(
        catchError(this.handleError('getStatus1', { status: 'offline' }))
      );
  }

  actualizarProducto(prod: Producto): Observable<any> {
    return this.http.post(this.baseUrl + '/api/productos/ModificarProducto', JSON.stringify(prod), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    })
      .pipe(
        catchError(this.handleError('getStatus1', { status: 'offline' }))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   * */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
