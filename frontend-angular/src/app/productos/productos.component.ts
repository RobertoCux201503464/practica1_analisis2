import { Component, OnInit } from '@angular/core';
import { Producto } from 'app/services/producto';
import { ProductoService } from 'app/services/producto.service';

@Component({
  selector: 'app-ecommerce',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {
    doubleSlider = [1000, 5000];
    focus;
    productos: Producto[];
    data : Date = new Date();

    constructor(private productService: ProductoService) { }

    ngOnInit() {
        var body = document.getElementsByTagName('body')[0];
        body.classList.add('ecommerce-page');
        this.productService.obtenerProductos().subscribe(res=>{
            this.productos = res;
        })
    }
    ngOnDestroy(){
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('ecommerce-page');
    }
}
