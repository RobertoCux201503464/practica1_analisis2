import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'app/services/auth.service';
import { Usuario } from 'app/services/usuario';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  focus_nombre: boolean;
  focus_contrasena: boolean;
  tipo_usuario: string = "cliente";
  nombre: string = '';
  contrasena: string = ''

  data: Date = new Date();

  constructor(private modalService: NgbModal, private authService: AuthService) { }

  ngOnInit() {
    var body = document.getElementsByTagName('body')[0];
    body.classList.add('signup-page');
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-absolute');
    navbar.classList.remove('fixed-top');

  }
  ngOnDestroy() {
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('signup-page');
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-absolute');
    navbar.classList.add('fixed-top');
  }
  
  registrar(): boolean {
    console.log({ nombre: this.nombre, contrasena: this.contrasena, tipo_usuario: this.tipo_usuario });
    if (this.nombre && this.nombre != '' && this.contrasena && this.contrasena != '') {
      if (this.tipo_usuario === 'cliente') {
        this.authService.registrarCliente(new Usuario(this.nombre, this.contrasena))
          .subscribe(res => {
            console.log(res);
            return true;
          });
      } else {
        this.authService.registrarUsuario(new Usuario(this.nombre, this.contrasena))
          .subscribe(res => {
            console.log(res);
            return true;
          });
      }
    }
    return false;
  }

  open(content: string) {
    this.modalService.open(content, { windowClass: 'modal-mini modal-primary', size: 'sm' }).result;
  }
}

