import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProductosComponent } from './productos/productos.component';
import { ProductpageComponent } from './productpage/productpage.component';
import { CreateProductComponent } from './create-product/create-product.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { CartComponent } from './cart/cart.component';
import { OrdenesComponent } from './ordenes/ordenes.component';

const routes: Routes =[
    { path: '', redirectTo: 'productos', pathMatch: 'full' },
    { path: 'productos',            component: ProductosComponent },
    { path: 'producto/:id',         component: ProductpageComponent },
    { path: 'crear-producto',       component: CreateProductComponent },
    { path: 'login',                component: LoginComponent },
    { path: 'register',             component: RegisterComponent },
    { path: 'editar-producto/:id',  component: EditProductComponent },
    { path: 'carrito',              component: CartComponent },
    { path: 'ordenes',              component: OrdenesComponent },
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    exports: [
    ],
})
export class AppRoutingModule { }
