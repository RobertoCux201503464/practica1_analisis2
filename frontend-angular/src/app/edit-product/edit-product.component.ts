import { Component, OnInit } from '@angular/core';
import * as Rellax from 'rellax';
import { Producto } from 'app/services/producto';
import { ProductoService } from 'app/services/producto.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {
  data: Date = new Date();
  focus;
  focus1;
  focus2;
  focus3;
  focus4;
  producto: Producto = new Producto();
  usuario = '';
  private routeSub: Subscription;
  constructor(private route: ActivatedRoute, private productService: ProductoService, private router: Router) { }

  ngOnInit() {
    if(localStorage.getItem('tipo_usuario') !== 'usuario') {
      this.router.navigate(['/']);
    }
    this.routeSub = this.route.params.subscribe((params: { id: number }) => {
      this.productService.obtenerProducto(params.id).subscribe(
        res=>{
          this.producto = res;
        }
      );
    });
    this.usuario = JSON.parse(localStorage.getItem('usuario')).nombre;
    var body = document.getElementsByTagName('body')[0];
    body.classList.add('contact-page');
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('contact-page');

  }

  actualizarProducto() {
    // console.log(this.producto);
    this.productService.actualizarProducto(this.producto).subscribe(res=>{
      alert('Producto Actualizado');
    });
  }

}
