import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Producto } from 'app/services/producto';
import { ProductoService } from 'app/services/producto.service';
import { Usuario } from 'app/services/usuario';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss']
})
export class CreateProductComponent implements OnInit {
  data: Date = new Date();
  focus;
  focus1;
  focus2;
  focus3;
  focus4;
  producto: Producto = new Producto();
  usuario = '';
  constructor(private productService: ProductoService, private router: Router) { }

  ngOnInit() {
    if(localStorage.getItem('tipo_usuario') !== 'usuario') {
      this.router.navigate(['/']);
    }
    
    this.usuario = JSON.parse(localStorage.getItem('usuario')).nombre;

    var body = document.getElementsByTagName('body')[0];
    body.classList.add('contact-page');
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('contact-page');
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');

  }

  registrarProducto() {
    console.log(this.producto);
    this.productService.registrarProducto(this.producto).subscribe(res=>{
      this.router.navigate(['/']);
    });
  }

}
