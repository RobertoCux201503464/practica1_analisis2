import { Component, OnInit } from '@angular/core';
import { Carrito } from 'app/Services/carrito';
import { OrdenService } from 'app/Services/orden.service';
import { Orden } from 'app/Services/orden';
import { Router } from '@angular/router';
import { Usuario } from 'app/services/usuario';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  carrito: Carrito = new Carrito();
  // usuario;
  constructor(private ordenService: OrdenService, private router: Router) { }

  ngOnInit() {
    if(localStorage.getItem('usuario') == null){
      this.router.navigate(['/']);
    }
    // this.usuario = JSON.parse(localStorage.getItem('usuario')) as Usuario;
    if(localStorage.getItem('carrito')!== null){
      this.carrito.elementos = JSON.parse(localStorage.getItem('carrito')).elementos;
    }
  }

  generarOrden(){
    const orden = new Orden();
    orden.total = this.carrito.total;
    orden.estado = 'pendente';
    orden.fecha = new Date(Date.now());
    orden.id_usuario = 1;
    orden.direccion = '';
    this.ordenService.crearOrden(orden).subscribe( res =>{
      alert("Orden generada con éxito")
    });
  }

}
