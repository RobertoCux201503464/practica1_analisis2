const express = require('express');
var request = require('request');
const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.json({ extended: false });

const app = express();
const port = 3000;

/**********************************************
 *           INICIAN RUTAS DE AUTH            *
 **********************************************/
app.post("/api/auth/client/login", urlencodedParser, (req, res) => {
    var opts = {
        uri: 'http://micro-servicio-auth/client/login',
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});

app.post("/api/auth/rest/login", urlencodedParser, (req, res) => {
    var opts = {
        uri: 'http://micro-servicio-auth/rest/login',
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});

app.post("/api/auth/client/registro", urlencodedParser, (req, res) => {
    var opts = {
        uri: 'http://micro-servicio-auth/client/registro',
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});

app.post("/api/auth/rest/registro", urlencodedParser, (req, res) => {
    var opts = {
        uri: 'http://micro-servicio-auth/rest/registro',
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});

app.get("/api/auth/client/users", urlencodedParser, (req, res) => {
    var opts = {
        uri: 'http://micro-servicio-auth/client/users',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});

app.get("/api/auth/rest/users", urlencodedParser, (req, res) => {
    var opts = {
        uri: 'http://micro-servicio-auth/rest/users',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});
/**********************************************
 *          FINALIZAN RUTAS DE AUTH           *
 **********************************************/

/**********************************************
 *         INICIAN RUTAS DE PRODUCTOS         *
 **********************************************/
app.post("/api/productos/Verproductos", urlencodedParser, (req, res) => {
    
    var opts = {
        uri: 'http://micro-servicio-productos/Verproductos',
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});

app.post("/api/productos/VerUnicoproducto", urlencodedParser, (req, res) => {
    
    var opts = {
        uri: 'http://micro-servicio-productos/VerUnicoproducto',
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});

app.post("/api/productos/IngresarProducto", urlencodedParser, (req, res) => {
    
    var opts = {
        uri: 'http://micro-servicio-productos/IngresarProducto',
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});

app.post("/api/productos/BorrarProducto", urlencodedParser, (req, res) => {
    
    var opts = {
        uri: 'http://micro-servicio-productos/BorrarProducto',
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});

app.post("/api/productos/ModificarProducto", urlencodedParser, (req, res) => {
    
    var opts = {
        uri: 'http://micro-servicio-productos/ModificarProducto',
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});
/**********************************************
 *        FINALIZAN RUTAS DE PRODUCTOS        *
 **********************************************/

/**********************************************
 *          INICIAN RUTAS DE ORDENES          *
 **********************************************/
app.get('/api/ordenes/', urlencodedParser, (req, res) => {
    var opts = {
        uri: 'http://micro-servicio-ordenes/',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});

app.put('/api/ordenes/actualizar_estado_orden', urlencodedParser, (req, res) => {
    var opts = {
        uri: 'http://micro-servicio-ordenes/actualizar_estado_orden',
        method: 'PUT',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});

app.put('/api/ordenes/actualizar_total_orden', urlencodedParser, (req, res) => {
    var opts = {
        uri: 'http://micro-servicio-ordenes/actualizar_total_orden',
        method: 'PUT',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});

app.delete('/api/ordenes/borrar_orden', urlencodedParser, (req, res) => {
    var opts = {
        uri: 'http://micro-servicio-ordenes/borrar_orden',
        method: 'DELETE',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});

app.post('/api/ordenes/crear_orden', urlencodedParser, (req, res) => {
    var opts = {
        uri: 'http://micro-servicio-ordenes/crear_orden',
        method: 'POST',
        body: JSON.stringify(req.body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});

app.get('/api/ordenes/leer_orden', urlencodedParser, (req, res) => {
    var opts = {
        uri: 'http://micro-servicio-ordenes/leer_orden',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});

app.post('/api/ordenes/leer_orden', urlencodedParser, (req, res) => {
    var opts = {
        uri: 'http://micro-servicio-ordenes/leer_orden',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(opts, function (error, response) {
        if (error) { return res.send({ status: 'error', details: error }); }
        console.log(error, response.body);
        return res.send(response.body);
    });
});

/**********************************************
 *         FINALIZAN RUTAS DE ORDENES         *
 **********************************************/

app.use(express.static('dist/frontend'));

app.all('/*', function (req, res, next) {
    // Just send the index.html for other files to support HTML5Mode
    res.sendFile('./dist/frontend/index.html', { root: __dirname });
});


app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})